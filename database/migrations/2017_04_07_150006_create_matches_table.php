<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('team_a_id')->index();
            $table->foreign('team_a_id')->references('id')->on('teams')->onDelete('cascade');
            $table->unsignedInteger('team_b_id')->index();
            $table->foreign('team_b_id')->references('id')->on('teams')->onDelete('cascade');
            $table->unsignedInteger('team_a_score')->default(0);
            $table->unsignedInteger('team_b_score')->default(0);
            $table->enum('status', ['in_process', 'paused', 'not_finished', 'finished'])->default('in_process')->index();
            $table->unsignedInteger('server_id')->index()->nullable();
            $table->foreign('server_id')->references('id')->on('servers')->onDelete('set null');
            $table->string('mode');
            $table->string('map');
            $table->time('duration')->default('00:00:00');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
