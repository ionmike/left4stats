<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('region');
            $table->string('name')->nullable();
            $table->ipAddress('ip');
            $table->unsignedInteger('port');
            $table->boolean('active')->default(true);
            $table->unsignedInteger('api_key_id')->index()->nullable();
            $table->foreign('api_key_id')->references('id')->on('api_keys')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servers');
    }
}
