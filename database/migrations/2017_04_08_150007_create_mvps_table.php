<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMvpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mvps', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('player_id')->index();
            $table->foreign('player_id')->references('id')->on('players')->onDelete('cascade');
            $table->unsignedInteger('match_id')->index();
            $table->foreign('match_id')->references('id')->on('matches')->onDelete('cascade');
            $table->unsignedInteger('damage');
            $table->unsignedInteger('si_kills');
            $table->unsignedInteger('ci_kills');
            $table->unsignedInteger('ff_damage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mvps');
    }
}
