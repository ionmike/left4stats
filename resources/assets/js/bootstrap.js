import lodash from 'lodash'
import vue from 'vue'
import axios from 'axios'
import Echo from 'laravel-echo'
import iziToast from 'izitoast'

window._ = lodash
window.Vue = vue
window.axios = axios
window.iziToast = iziToast

window.axios.defaults.headers.common['X-CSRF-TOKEN'] = window.Laravel.csrfToken;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001'
});
