
import './bootstrap'
import moment from 'moment'
import VueProgressBar from 'vue-progressbar'

import router from './routes'

Vue.prototype.$moment = moment
Vue.prototype.$eventBus = new Vue()

Vue.use(VueProgressBar, {
    color: 'rgb(0, 209, 178)',
    failedColor: 'red',
    height: '1px'
})

import Navbar from './components/Navbar'

const app = new Vue({
    el: '#app',

    components: {
        Navbar
    },

    router
})
