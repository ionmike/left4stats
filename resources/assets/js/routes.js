
import VueRouter from 'vue-router'

// Common views
import Matches from './views/Matches'

const routes = [
    {
        path: '/',
        redirect: { name: 'matches' }
    },
    {
        name: 'matches',
        path: '/matches',
        component: Matches
    }
]

export default new VueRouter({
    routes,
    linkActiveClass: 'is-active'
})